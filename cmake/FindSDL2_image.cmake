# Depends on SDL2
find_package(SDL2 REQUIRED)
set(SDL2_image_INCLUDE_DIR ${SDL2_INCLUDE_DIR})

find_library(
    SDL2_image_LIBRARY
    NAMES SDL2_image
    HINTS
        ENV SDL2DIR
    PATH_SUFFIXES lib lib64
    CACHE STRING "Where the SDL2_image library is"
)

include(${CMAKE_ROOT}/Modules/FindPackageHandleStandardArgs.cmake)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(
    SDL2_image
    REQUIRED_VARS SDL2_image_LIBRARY SDL2_image_INCLUDE_DIR
)
