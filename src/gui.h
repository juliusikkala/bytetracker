/*
    Copyright 2015 Julius Ikkala

    This file is part of ByteTracker.

    ByteTracker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ByteTracker is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ByteTracker.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef BYTETRACKER_GUI_H_
#define BYTETRACKER_GUI_H_
    #include "sdl.h"
    #include <stdlib.h>
    #include <stdint.h>
    
    enum gui_object_type
    {
        GUI_PANEL=0,
        GUI_BUTTON,
        GUI_IMAGE
    };
    
    enum gui_event_type
    {
        GUI_MOUSE_PRESS=0,
        GUI_MOUSE_RELEASE,
        GUI_MOUSE_SCROLL,
        GUI_MOUSE_ENTER,
        GUI_MOUSE_LEAVE
    };
    enum gui_mouse_button
    {
        GUI_BUTTON_LEFT,
        GUI_BUTTON_MIDDLE,
        GUI_BUTTON_RIGHT
    };
    
    union gui_event
    {
        enum gui_event_type type;
        /*GUI_MOUSE_ENTER ja GUI_MOUSE_EXIT eivät saa mitään lisätietoja.*/

        struct
        {/*GUI_MOUSE_PRESS, GUI_MOUSE_RELEASE*/
            enum gui_event_type t;
            enum gui_mouse_button btn;
        } mouse_button;
        
        struct
        {/*GUI_MOUSE_SCROLL*/
            enum gui_event_type t;
            int x, y;
        } mouse_scroll;
    };
    
    typedef void (*gui_event_callback)(
        void* userdata,
        unsigned id,
        union gui_event
    );
    
    /*Pääobjekti on paneeli. Kivasti rekursiivista, eikö?*/
    struct gui_panel;
    struct gui_object;
    typedef uint32_t gui_colour;
    struct gui_theme
    {
        gui_colour panel_colour;
        struct
        {
            image down;
            image up;
            /*Koko nappikuvan koko.*/
            unsigned proto_w, proto_h;
            /*Jokaisen nurkan tulee olla saman kokoinen. Loppuosaa napin kuvasta
              käytetään napin reunana.*/
            unsigned corner_w, corner_h;
        } button;
    };
    /*struct gui_button;
    struct gui_label;
    struct gui_image;*/
    
    /*Luo 'standalone' paneelin, id 0*/
    struct gui_panel* gui_create_panel(
        unsigned w,
        unsigned h,
        struct gui_theme* t
    );
    
    void gui_free_panel(struct gui_panel* p);
    
    struct gui_object* gui_panel_get(struct gui_panel* p, unsigned id);
    void gui_panel_remove(struct gui_panel* p, unsigned id);
    /*Poistaa kaikki objectit*/
    void gui_panel_clear(struct gui_panel* p);
    
    /*Palauttaa id:n, tai nollan virheessä.*/
    unsigned gui_push_button(
        struct gui_panel* p,
        unsigned x, unsigned y,
        unsigned w, unsigned h,/*Aseta nollaan, jos haluat automaattisen leveyden*/
        gui_event_callback cb,/*Aseta NULL:iin jos haluat ilman callbackia*/
        unsigned id,/*Aseta nollaan, jos haluat funktion keksivän id:n itse*/
        image i/*Lainataan, voi olla NULL*/
    );
    
    unsigned gui_push_panel(
        struct gui_panel* p,
        unsigned x, unsigned y,
        unsigned w, unsigned h,/*Ei automaattista leveyttä*/
        gui_event_callback cb,
        unsigned id
    );
    
    unsigned gui_push_image(
        struct gui_panel* p,
        unsigned x, unsigned y,
        unsigned w, unsigned h,
        gui_event_callback cb,
        unsigned id,
        image i/*Lainataan*/
    );
    
    void draw_gui(struct gui_panel* p, SDL_Renderer* ren);
    /*Userdata tarjotaan, jotta käyttäjä voisi välttää globaaleja.*/
    void gui_handle_event(struct gui_panel* p, SDL_Event event, void* userdata);
#endif
