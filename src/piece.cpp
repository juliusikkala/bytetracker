/*
    Copyright 2015 Julius Ikkala

    This file is part of ByteTracker.

    ByteTracker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ByteTracker is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ByteTracker.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "piece.h"
#include "sheet.h"
#include "effect.h"
#include "io.h"
#include "str.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>/*sprintf*/

/*Palauttaa ei-nollan, jos tulee virhe. Kirjoittaa virheen piece_error:iin*/
unsigned create_piece(
    struct piece* piece,
    const char* const* sheet_paths,
    /*Lähteet, voi olla NULL. Sitten tämä funktio yrittää ladata itse
      sheet_paths:sista.*/
    const char* const* sheet_srcs,
    unsigned sheet_amount,
    unsigned* error_sheet_index,
    unsigned* error_line
)
{
    unsigned i=0;
    enum length_type len_type=LENGTH_EXP;
    piece->sheets=(struct sheet*)malloc(sizeof(struct sheet)*sheet_amount);
    piece->sheets_sz=sheet_amount;
    for(i=0;i<sheet_amount;i++)
    {
        char* allocated_src=NULL;
        const char* src=NULL;
        unsigned status=0;
        if(sheet_srcs==NULL)
        {
            allocated_src=read_text_file(sheet_paths[i]);
            src=allocated_src;
            if(src==NULL)
            {
                free(piece->sheets);
                if(error_sheet_index!=NULL)
                {
                    *error_sheet_index=i;
                }
                if(error_line!=NULL)
                {
                    *error_line=0;
                }
                return 1;
            }
        }
        else
        {
            src=sheet_srcs[i];
        }
        status=sheet_read(piece->sheets+i, src, error_line);
        if(allocated_src!=NULL)
        {
            free(allocated_src);
            allocated_src=NULL;
        }
        if(status!=0)
        {
            free(piece->sheets);
            piece->sheets=NULL;
            if(error_sheet_index!=NULL)
            {
                *error_sheet_index=i;
            }
            return status;
        }
        if(len_type!=LENGTH_ABS)
        {
            if(sheet_can_convert_length_type(
                piece->sheets+i,
                LENGTH_ABS,
                len_type
            )!=0)
            {
                len_type=LENGTH_ABS;
            }
        }
    }
    /*Sheetit on nyt onnellisesti ladattu.*/
    if(len_type!=LENGTH_ABS)
    {/*Muunnetaan kaikki sheetit haluttuun formaattiin*/
        for(i=0;i<sheet_amount;i++)
        {
            sheet_convert_length_type(piece->sheets+i, LENGTH_ABS, len_type);
        }
    }
    create_note_format(
        &(piece->format),
        len_type
    );
    return 0;
}
struct piece_sources
{
    char **name;
    char **src;
    unsigned amount;
};
static void free_piece_sources(struct piece_sources ps)
{
    unsigned i=0;
    if(ps.name!=NULL||ps.name!=NULL)
    {
        for(i=0;i<ps.amount;i++)
        {
            free(ps.name[i]);
            free(ps.src[i]);
        }
        free(ps.name);
        free(ps.src);
    }
}
static char* read_attribute(char* feed, struct piece_sources* ps, struct piece* piece, const char* path_prefix)
{
    unsigned i=0;
    SKIP_WHITESPACE(feed);
    if(strncmp(feed, "SHEET", 5)==0)
    {
        char* path=NULL;
        char* src=NULL;
        char* path_begin=NULL;
        unsigned path_len=0;

        feed+=5;
        ENSURE_SKIP_WHITESPACE(feed);
        path_len=strcspn(feed, " \t\r\n");
        path_begin=feed;
        
        feed+=path_len;
        SKIP_WHITESPACE(feed);
        if(*feed!='\n')
        {
            return NULL;
        }
        /*Lue path*/
        path=(char*)malloc(path_len+7+(path_prefix==NULL?0:strlen(path_prefix)));
        if(path_prefix!=NULL)
        {
            sprintf(path, "%s%.*s.sheet", path_prefix, path_len, path_begin);
        }
        else
        {
            sprintf(path, "%.*s.sheet", path_len, path_begin);
        }
        /*Lataa lähde*/
        src=read_text_file(path);
        if(src==NULL)
        {
            free(path);
            return NULL;
        }
        ps->amount++;
        ps->name=(char**)realloc(ps->name, ps->amount*sizeof(char*));
        ps->src=(char**)realloc(ps->src, ps->amount*sizeof(char*));
        ps->name[ps->amount-1]=path;
        ps->src[ps->amount-1]=src;
        return feed;
    }
    else if(strncmp(feed, "EFFECT", 6)==0)
    {
        unsigned effect_index=0;
        union effect res;
        
        feed+=6;
        ENSURE_SKIP_WHITESPACE(feed);
        READ_INT(feed, effect_index);
        ENSURE_SKIP_WHITESPACE(feed);
        
        for(i=0;i<sizeof(effect_names)/sizeof(const char**);i++)
        {
            if(strncmp(feed, effect_names[i], strlen(effect_names[i]))==0)
            {
                feed+=strlen(effect_names[i]);
                if(*feed!='\n')
                {
                    ENSURE_SKIP_WHITESPACE(feed);
                }
                feed=effect_fns[i](&res, feed);
                if(feed!=NULL)
                {
                    SKIP_WHITESPACE(feed);
                    if(*feed!='\n')
                    {/*Roskaa rivin lopussa*/
                        free_effect(&res);
                        return NULL;
                    }
                    feed++;
                    note_format_set_effect(
                        &(piece->format),
                        effect_index,
                        res
                    );
                }
                return feed;
            }
        }
        return NULL;
    }
    else
    {
        return NULL;
    }
    return NULL;
}

/*Lataa piece:n  tekstitiedostosta. Palauttaa ei-nollan, jos tulee virhe.
  Kirjoittaa virheen piece_error:iin*/
/*Piecen syntaksi on seuraava:
  ...
  ATTRIBUUTTI ARVO1 ARVO2 ... ARVON
  //kommentti
  ...
  ATTRIBUUTTI on
  -EFFECT
    -Arvot ovat: NUMERO TYYPPI ARGUMENTTI1 ARGUMENTTI2 ...
    -NUMERO on efektin numero sheeteissä
    -TYYPPI on efektin tyyppi
    -ARGUMENTTIN on tyypin vaatima argumentti.
  -SHEET
    -Arvo on sheet-tiedoston nimi ilman päätettä.
*/
unsigned piece_read(
    struct piece* piece,
    const char* piece_src,
    const char* path_prefix,
    unsigned* error_line,
    unsigned* error_sheet_index,
    unsigned* error_sheet_line
)
{
    char *feed=(char*)malloc(strlen(piece_src)+2);
    char *feed_begin=feed;
    char *tmp=feed;
    struct piece_sources srcs;
    struct note_format fmt_tmp;
    unsigned line=0;
    srcs.name=NULL;
    srcs.src=NULL;
    srcs.amount=0;
    
    /*Lisätään newline loppuun, niin funktioiden ei tarvitse tarkistaa
      nollaa*/
    sprintf(feed, "%s\n", piece_src);
    
    create_note_format(
        &piece->format,
        LENGTH_EXP
    );
    
    piece->sheets=NULL;
    piece->sheets_sz=0;
    
    remove_comments(feed);
    /*printf("%s\n", feed);*/
    while(feed!=NULL&&*feed!=0)
    {
        line++;
        /*Skipataan sekä whitespace että newlinet*/
        SKIP_WHITESPACE(feed);
        if(*feed=='\n')
        {
            feed++;
            continue;
        }
        
        if((tmp=read_attribute(feed, &srcs, piece, path_prefix))!=NULL)
        {
            feed=tmp;
            continue;
        }
        else
        {
            goto read_fail;
        }
    }
    fmt_tmp=piece->format;
    line=0;
    if(create_piece(
        piece,
        (const char * const *)srcs.name,
        (const char * const *)srcs.src,
        srcs.amount,
        error_sheet_index,
        error_sheet_line
    )!=0)
    {
        goto read_fail;
    }
    fmt_tmp.len_type=piece->format.len_type;
    free_note_format(&(piece->format));
    piece->format=fmt_tmp;
    free_piece_sources(srcs);
    free(feed_begin);
    return 0;
read_fail:
    if(error_line!=NULL)
    {
        *error_line=line;
    }
    free_piece_sources(srcs);
    free_piece(piece);
    free(feed_begin);
    return 1;
}
/*Vapauttaa kaiken piece:n käyttämän muistin. Kutsu tämä, kun pieceä ei enää
  käytetä*/
void free_piece(struct piece* piece)
{
    free_note_format(&(piece->format));
    if(piece->sheets!=NULL)
    {
        unsigned i=0;
        for(i=0;i<piece->sheets_sz;i++)
        {
            free_sheet(piece->sheets+i);
        }
        free(piece->sheets);
        piece->sheets=NULL;
        piece->sheets_sz=0;
    }
}

enum validate_error piece_validate(
    struct piece* piece,
    unsigned* error_sheet_index,
    const struct note_bits nb,
    unsigned* error_line
)
{
    unsigned i=0;
    enum validate_error err=VALIDATE_NO_ERROR;
    for(i=0;i<piece->sheets_sz;i++)
    {
        if((err=sheet_validate(piece->sheets+i, &(piece->format), nb, error_line))!=VALIDATE_NO_ERROR)
        {
            if(error_sheet_index!=NULL)
            {
                *error_sheet_index=i;
            }
            return err;
        }
    }
    return err;
}

