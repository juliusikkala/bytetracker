/*
    Copyright 2015 Julius Ikkala

    This file is part of ByteTracker.

    ByteTracker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ByteTracker is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ByteTracker.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "sdl.h"
#include "cmakeconfig.h"
#include <SDL2/SDL_image.h>

/*Lataa SDL2:n ja kuvaresurssit. Ei lataa sampleja, se tehdään audio.h:ssa.
  Palauttaa ei-nollan jos epäonnistuu, printtaa itse virheen.*/
int init_sdl(struct sdl_resources *res, unsigned w, unsigned h)
{
    unsigned flags;
    /*Nollataan heti alkuun, niin quit_sdl:n voi kutsua turvallisesti vaikka tämä
      funktio epäonnistuisi (sitä ei periaatteessa tarvitsisi kutsua)*/
    res->win=NULL;
    res->ren=NULL;
    if(SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO|SDL_INIT_EVENTS)!=0)
    {
        SDL_ShowSimpleMessageBox(
            SDL_MESSAGEBOX_ERROR,
            "SDL:n initialisointi epäonnistui",
            SDL_GetError(),
            NULL
        );
        return 1;
    }
    res->win=SDL_CreateWindow(
        "ByteTracker " BYTETRACKER_VERSION_STR " - " BYTETRACKER_VERSION_CODENAME,
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        w, h,
        0/*Flagit, ei tarvita nyt.*/
    );
    if(res->win==NULL)
    {
        SDL_ShowSimpleMessageBox(
            SDL_MESSAGEBOX_ERROR,
            "Ikkunan avaaminen epäonnistui",
            SDL_GetError(),
            NULL
        );
        goto init_fail;
    }
    res->ren=SDL_CreateRenderer(res->win, -1, SDL_RENDERER_PRESENTVSYNC);
    if(res->ren==NULL)
    {
        SDL_ShowSimpleMessageBox(
            SDL_MESSAGEBOX_ERROR,
            "Renderöijän luonti epäonnistui",
            SDL_GetError(),
            NULL
        );
        goto init_fail;
    }
    /*Enabletaan Drag & Drop*/
    SDL_EventState(SDL_DROPFILE, SDL_ENABLE);
    /*Kuvat*/
    flags=IMG_INIT_PNG;
    if((IMG_Init(flags)&flags)!=flags)
    {
        SDL_ShowSimpleMessageBox(
            SDL_MESSAGEBOX_ERROR,
            "Kuvakirjaston initialisointi epäonnistui",
            IMG_GetError(),
            NULL
        );
        goto init_fail;
    }
    return 0;
init_fail:
    if(res->win!=NULL)
    {
        SDL_DestroyWindow(res->win);
        res->win=NULL;
    }
    if(res->ren!=NULL)
    {
        SDL_DestroyRenderer(res->ren);
        res->ren=NULL;
    }
    SDL_Quit();
    return 1;
}

void quit_sdl(struct sdl_resources *res)
{
    IMG_Quit();
    if(res->ren!=NULL)
    {
        SDL_DestroyRenderer(res->ren);
        res->ren=NULL;
    }
    if(res->win!=NULL)
    {
        SDL_DestroyWindow(res->win);
        res->win=NULL;
    }
    SDL_Quit();
}

void set_window_icon(struct sdl_resources *res, const char* icon_path)
{
    SDL_Surface *icon_img=IMG_Load(icon_path);
    if(icon_img!=NULL)
    {
        SDL_SetWindowIcon(res->win, icon_img);
    }
    SDL_FreeSurface(icon_img);
}
image load_image(const char* path, struct sdl_resources* res)
{
    return IMG_LoadTexture(res->ren, path);
}
void free_image(image i)
{
    if(i!=NULL)
    {
        SDL_DestroyTexture(i);
    }
}

int image_get_size(image i, unsigned* w, unsigned* h)
{
    if(i==NULL||SDL_QueryTexture(i, NULL, NULL, (int*)w, (int*)h)!=0)
    {
        return 1;
    }
    return 0;
}
