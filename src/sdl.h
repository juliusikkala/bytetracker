/*
    Copyright 2015 Julius Ikkala

    This file is part of ByteTracker.

    ByteTracker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ByteTracker is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ByteTracker.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef BYTETRACKER_SDL_H_
#define BYTETRACKER_SDL_H_
    #include <SDL2/SDL.h>
    struct sdl_resources{
        SDL_Window *win;
        SDL_Renderer *ren;
    };
    /*Lataa SDL2:n ja kuvaresurssit. Ei lataa sampleja, se tehdään audio.h:ssa.
      Palauttaa ei-nollan jos epäonnistuu, printtaa itse virheen.*/
    int init_sdl(struct sdl_resources *res, unsigned w, unsigned h);
    void quit_sdl(struct sdl_resources *res);
    
    void set_window_icon(struct sdl_resources *res, const char* icon_path);
    
    typedef SDL_Texture* image;
    
    image load_image(const char* path, struct sdl_resources* res);
    void free_image(image i);
    
    /*Palauttaa ei-nollan jos epäonnistuu*/
    int image_get_size(image i, unsigned* w, unsigned* h);
#endif
