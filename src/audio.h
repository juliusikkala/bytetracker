/*
    Copyright 2015 Julius Ikkala

    This file is part of ByteTracker.

    ByteTracker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ByteTracker is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ByteTracker.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef BYTETRACKER_AUDIO_H_
#define BYTETRACKER_AUDIO_H_
    #include "fp.h"
    typedef fp::q<28, int64_t> phase_type;
    struct piece;
    
    struct audio_object;
    
    /*Julkinen vain, koska effect.c/h tarvitsee sitä*/
    struct sheet_audio_info
    {
        unsigned note_index;
        unsigned note_time;/*Sampleina*/
        unsigned note_length;
        phase_type phase;
        float last_sample;
    };
    struct audio_info
    {
        unsigned t;
        unsigned samplerate;
    };
    
    /*Objekti on automaattisesti pausella, kun halutaan ääntä, kutsu
      pause_audio(obj, 0);*/
    struct audio_object* begin_audio(struct piece* p);
    /*Jos pause on 0, niin ääni jatkuu, muutoin se menee pauselle.*/
    void pause_audio(struct audio_object* obj, unsigned pause);
    /*Saattaa audio_object:in alkutilaan.*/
    void rewind_audio_object(struct audio_object* obj);
    
    void audio_set_volume(struct audio_object* obj, float volume);
    /*Tuhoaa audio_object:in*/
    void stop_audio(struct audio_object* obj);

    float calculate_frequency(float base_freq, float semitone);
#endif
