/*
    Copyright 2015 Julius Ikkala

    This file is part of ByteTracker.

    ByteTracker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ByteTracker is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ByteTracker.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef BYTETRACKER_INSTRUMENT_H_
#define BYTETRACKER_INSTRUMENT_H_
    #include "audio.h"
    enum instrument
    {
        INSTRUMENT_SINE=0,
        INSTRUMENT_TRIANGLE,
        INSTRUMENT_SQUARE,
        INSTRUMENT_SAWTOOTH,
        INSTRUMENT_NOISE,
        INSTRUMENT_ORGAN,
        INSTRUMENT_DARKSYNTH
    };
    /*Kaikki argumentit sekunneissa.*/
    typedef float (*instrument_fn)(phase_type phase);
    extern const char *const instrument_names[7];
    extern const instrument_fn instrument_fns[7];
#endif
