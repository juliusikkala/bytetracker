/*
    Copyright 2015 Julius Ikkala

    This file is part of ByteTracker.

    ByteTracker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ByteTracker is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ByteTracker.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef BYTETRACKER_EFFECT_H_
#define BYTETRACKER_EFFECT_H_
    #include <stdint.h>
    #include "instrument.h"
    
    struct sheet;
    struct sheet_audio_info;
    struct audio_info;
    
    enum effect_type
    {
        EFFECT_NONE=0,
        EFFECT_VIBRATO,
        EFFECT_VOLUME,
        EFFECT_PIZZICATO,
        EFFECT_GLISSANDO,
        EFFECT_ARPEGGIO
    };
    
    union effect
    {
        enum effect_type type;
        struct
        {
            enum effect_type pad;
            float rate;
            float extent;
        } vibrato;
        struct
        {
            enum effect_type pad;
            float vol;
        } volume;
        struct
        {
            enum effect_type pad;
            float attack_time;
            float hold_time;
            float falloff_time;
        } pizzicato;
        struct
        {
            enum effect_type pad;
            float time;
        } glissando;
        struct
        {
            enum effect_type pad;
            float rate;
            signed *semitones;/*Signed, jotta voisi pyöriä alhaallakin*/
            unsigned semitones_sz;
        } arpeggio;
    };
    /*Palauttaa NULL, jos epäonnistuu, muutoin osoittaa ensimmäiseen merkkiin
      argumenttien jälkeen.*/
    typedef char* (*create_effect_fn)(union effect* eff, char* args);
    extern const char *const effect_names[6];
    extern const create_effect_fn effect_fns[6];
    
    /*Palauttaa fn(phase), kun efekti on EFFECT_NONE, muutoin käyttää määritettyä
      efektiä. Argumentiksi tarvitaan sheet esim. glissandon vuoksi.*/
    float get_sample(
        const union effect* eff,
        const struct sheet *sheet,
        struct sheet_audio_info* sheet_audio_info,
        const struct audio_info* audio_info
    );
    
    void free_effect(union effect* eff);
#endif
