/*
    Copyright 2015 Julius Ikkala

    This file is part of ByteTracker.

    ByteTracker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ByteTracker is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ByteTracker.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef BYTETRACKER_STR_H_
#define BYTETRACKER_STR_H_
    #include <string.h>
    /*Joitakin työkaluja tekstin käsittelyyn.*/
    #define LINE_COMMENT "//"
    #define BEGIN_COMMENT "/*"
    #define END_COMMENT "*/"
    #define SKIP_WHITESPACE(feed) ((feed)+=strspn((feed), " \r\t"))
    /*Varmistaa, että jonkin verran whitespacea ollaan skipattu*/
    #define ENSURE_SKIP_WHITESPACE(feed) do{\
        unsigned tmp=strspn((feed), " \r\t");\
        if(tmp==0)\
        {\
            return NULL;\
        }\
        (feed)+=tmp;}while(0)
    #define READ_INT(feed, var) \
        if(strcspn((feed), "-1234567890")!=0) \
        {\
            return NULL;\
        }\
        (var)=strtol((feed),&(feed),0);
    #define READ_INT_NORET(feed, var, err) \
        if(strcspn((feed), "-1234567890")!=0) \
        {\
            (err)=1;\
        }\
        else \
        { \
            (var)=strtol((feed),&(feed),0);\
            (err)=0;\
        }
    #define READ_UNSIGNED(feed, var) \
        if(strcspn((feed), "1234567890")!=0) \
        {\
            return NULL;\
        }\
        (var)=strtol((feed),&(feed),0);
    #define READ_UNSIGNED_FRACTION(feed, numer, denom)\
        do{\
            char* peek=NULL;\
            READ_UNSIGNED((feed), (numer));\
            peek=(feed);\
            SKIP_WHITESPACE(peek);\
            if(*peek=='/')\
            {\
                (feed)=peek+1;\
                SKIP_WHITESPACE((feed));\
                READ_UNSIGNED((feed), (denom));\
            }\
            else\
            {\
                (denom)=1;\
            }\
        }while(0)
    #define READ_FRACTION(feed, numer, denom)\
        do{\
            char* peek=NULL;\
            READ_INT((feed), (numer));\
            peek=(feed);\
            SKIP_WHITESPACE(peek);\
            if(*peek=='/')\
            {\
                (feed)=peek+1;\
                SKIP_WHITESPACE((feed));\
                READ_INT((feed), (denom));\
            }\
            else\
            {\
                (denom)=1;\
            }\
        }while(0)
    /*Jättää newlinet, jotta rivien laskenta ei pilaantuisi.*/
    void remove_comments(char* feed);
#endif
