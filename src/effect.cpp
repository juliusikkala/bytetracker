/*
    Copyright 2015 Julius Ikkala

    This file is part of ByteTracker.

    ByteTracker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ByteTracker is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ByteTracker.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "effect.h"
#include "audio.h"
#include "sheet.h"
#include "instrument.h"
#include "str.h"
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif
#define min(a,b) ((a)<(b)?(a):(b))
#define max(a,b) ((a)>(b)?(a):(b))
#define lerp(a, b, t) ((1-(t))*(a)+(t)*(b))

const char *const effect_names[6]={
    "NONE",
    "VIBRATO",
    "VOLUME",
    "PIZZICATO",
    "GLISSANDO",
    "ARPEGGIO"
};

char *create_effect_none(union effect* eff, char* args)
{
    eff->type=EFFECT_NONE;
    return args;
}
char *create_effect_vibrato(union effect* eff, char* args)
{
    unsigned rate_n=0, rate_d=1;
    unsigned extent_n=0, extent_d=1;
    eff->type=EFFECT_VIBRATO;
    READ_UNSIGNED_FRACTION(args, rate_n, rate_d);
    SKIP_WHITESPACE(args);
    READ_UNSIGNED_FRACTION(args, extent_n, extent_d);
    eff->vibrato.rate=(double)rate_n/rate_d;
    eff->vibrato.extent=(double)extent_n/extent_d;
    return args;
}
char *create_effect_volume(union effect* eff, char* args)
{
    unsigned n=0, d=1;
    eff->type=EFFECT_VOLUME;
    READ_FRACTION(args, n, d);
    eff->volume.vol=(double)n/d;
    return args;
}
char *create_effect_pizzicato(union effect* eff, char* args)
{
    unsigned n=0, d=1;
    eff->type=EFFECT_PIZZICATO;
    READ_FRACTION(args, n, d);
    SKIP_WHITESPACE(args);
    eff->pizzicato.attack_time=(double)n/d;
    READ_FRACTION(args, n, d);
    SKIP_WHITESPACE(args);
    eff->pizzicato.hold_time=(double)n/d;
    READ_FRACTION(args, n, d);
    eff->pizzicato.falloff_time=(double)n/d;
    return args;
}
char *create_effect_glissando(union effect* eff, char* args)
{
    unsigned n=0, d=1;
    eff->type=EFFECT_GLISSANDO;
    READ_FRACTION(args, n, d);
    eff->glissando.time=(double)n/d;
    return args;
}
char *create_effect_arpeggio(union effect* eff, char* args)
{
    unsigned n=0, d=0;
    eff->type=EFFECT_ARPEGGIO;
    READ_UNSIGNED_FRACTION(args, n, d);
    SKIP_WHITESPACE(args);
    eff->arpeggio.rate=n/(double)d;
    eff->arpeggio.semitones=NULL;
    
    for(eff->arpeggio.semitones_sz=0;*args!='\n';eff->arpeggio.semitones_sz++)
    {
        signed n=0;
        unsigned err=1;
        READ_INT_NORET(args, n, err);
        if(err==1)
        {
            if(eff->arpeggio.semitones!=NULL)
            {
                free(eff->arpeggio.semitones);
                eff->arpeggio.semitones=NULL;
            }
            return NULL;
        }
        eff->arpeggio.semitones=(int*)realloc(
            eff->arpeggio.semitones,
            (eff->arpeggio.semitones_sz+1)*sizeof(signed)
        );
        eff->arpeggio.semitones[eff->arpeggio.semitones_sz]=n;
        SKIP_WHITESPACE(args);
    }
    return args;
}
const create_effect_fn effect_fns[6]={
    create_effect_none,
    create_effect_vibrato,
    create_effect_volume,
    create_effect_pizzicato,
    create_effect_glissando,
    create_effect_arpeggio
};

float get_sample(
    const union effect* eff,
    const struct sheet *sheet,
    struct sheet_audio_info* sheet_audio_info,
    const struct audio_info* audio_info
)
{
    float f=0;
    float semitone=0;
    float volume=1.0;
    double time=audio_info->t/(double)audio_info->samplerate;
    double note_time=sheet_audio_info->note_time/(double)audio_info->samplerate;
    double note_length=sheet_audio_info->note_length/(double)audio_info->samplerate;
    double unit_length=sheet->note_info.unit_length_numer/(double)sheet->note_info.unit_length_denom;
    const unsigned* st=sheet_get_semitone(sheet, sheet_audio_info->note_index);
    if(st==NULL)/*Nuotti on hiljennetty tai tuli muu virhe*/
    {
        return sheet_audio_info->last_sample;
    }
    semitone=*st;
    if(eff!=NULL)
    {
        switch(eff->type)
        {
        case EFFECT_VIBRATO:
            semitone+=
                eff->vibrato.extent*sin(2*M_PI*eff->vibrato.rate*time/unit_length);
            break;
        case EFFECT_VOLUME:
            volume=eff->volume.vol;
            break;
        case EFFECT_PIZZICATO:
            if(note_time<eff->pizzicato.attack_time*unit_length)
            {
                volume=note_time/(eff->pizzicato.attack_time*unit_length);
            }
            else if(note_time>(eff->pizzicato.hold_time+eff->pizzicato.attack_time)*unit_length)
            {
                volume=max(1.0-(note_time-(eff->pizzicato.hold_time+eff->pizzicato.attack_time)*unit_length)/
                    (eff->pizzicato.falloff_time*unit_length), 0);
            }
            break;
        case EFFECT_GLISSANDO:
            #define t (max(0, note_time-(note_length-eff->glissando.time*unit_length))/(eff->glissando.time*unit_length))
            st=sheet_get_semitone(sheet, sheet_audio_info->note_index+1);
            if(st!=NULL)
            {
                semitone=lerp(
                    semitone,
                    *st,
                    t
                );
            }
            else
            {
                volume=volume*(1-t);
            }
            #undef t
            break;
        case EFFECT_ARPEGGIO:
            semitone+=eff->arpeggio.semitones[
                (int)(eff->arpeggio.rate*time/unit_length)%eff->arpeggio.semitones_sz
            ];
            break;
        default:
            break;
        }
    }
    
    f=calculate_frequency(
        sheet->note_info.base_freq,
        semitone
    );
    sheet_audio_info->phase+=phase_type(f/audio_info->samplerate);
    //sheet_audio_info->phase=fmod(sheet_audio_info->phase, 2.0);
    return (sheet_audio_info->last_sample=instrument_fns[sheet->note_info.ins](
        sheet_audio_info->phase
    )*volume);
}

void free_effect(union effect* eff)
{
    switch(eff->type)
    {
    case EFFECT_ARPEGGIO:
        if(eff->arpeggio.semitones!=NULL)
        {
            free(eff->arpeggio.semitones);
            eff->arpeggio.semitones=NULL;
        } 
        break;
    default:
        break;
    }
    eff->type=EFFECT_NONE;
}
