/*
    Copyright 2015 Julius Ikkala

    This file is part of ByteTracker.

    ByteTracker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ByteTracker is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ByteTracker.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "sheet.h"
#include "audio.h"
#include "note.h"
#include "instrument.h"
#include "effect.h"
#include "str.h"
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#define SEPARATOR ':'
#define BUCKET_SIZE 128
#define SYMBOL_A 'A'
#define SYMBOL_H 'H'
#define SYMBOL_C 'C'
#define SYMBOL_D 'D'
#define SYMBOL_E 'E'
#define SYMBOL_F 'F'
#define SYMBOL_G 'G'
#define MOD_SHARP '#'
#define MOD_FLAT 'b'

struct proto_note
{
    signed semitone;/*440Hz on pohjana tässä, siksi signed. Tästä muunnetaan sitten oikeaan nuottiin*/
    unsigned silent;/*Ääntä ei kuulu, kun tämä on päällä*/
    unsigned effect;
    unsigned length;
};
/*read_note:lle omia muuttujia, joita tarvitsee säilyttää yli funktiokutsujen.*/
struct read_note_context
{
    unsigned last_effect;
};
static char* read_note(
    char* feed,
    struct read_note_context* ctx,
    struct proto_note* note
)
{
    signed octave=0;
    SKIP_WHITESPACE(feed);
    /*Luetaan ensin nuotti*/
    switch(*feed)
    {
    case SEPARATOR:
        note->silent=1;
        break;
    case SYMBOL_A:
        note->semitone=0;
        goto case_note;/*goto considered useful ;)*/
    case SYMBOL_H:
        note->semitone=2;
        goto case_note;
    case SYMBOL_C:
        note->semitone=-9;
        goto case_note;
    case SYMBOL_D:
        note->semitone=-7;
        goto case_note;
    case SYMBOL_E:
        note->semitone=-5;
        goto case_note;
    case SYMBOL_F:
        note->semitone=-4;
        goto case_note;
    case SYMBOL_G:
        note->semitone=-2;
        goto case_note;
    default:
        return NULL;
    case_note:
        note->silent=0;
        feed++;
        /*Ylennetyn ja alennetun merkki, jos on*/
        switch(*feed)
        {
        case MOD_SHARP:
            note->semitone++;
            feed++;
            break;
        case MOD_FLAT:
            note->semitone--;
            feed++;
            break;
        default:
            break;
        }
        /*Sitten oktaavin numero*/
        READ_INT(feed, octave);
        note->semitone+=(octave-4)*12;
        SKIP_WHITESPACE(feed);
        if(*feed!=SEPARATOR)
        {
            return NULL;
        }
        break;
    }
    feed++;/*Hypätään erotin*/
    SKIP_WHITESPACE(feed);
    /*Sitten luetaan pituus*/
    READ_INT(feed, note->length);
    SKIP_WHITESPACE(feed);
    if(*feed!=SEPARATOR||note->length==0)
    {
        return NULL;
    }
    feed++;
    SKIP_WHITESPACE(feed);
    /*Sitten efekti*/
    if(*feed=='\n')
    {/*Efektiä ei määritelty*/
        note->effect=ctx->last_effect;
        return ++feed;
    }
    READ_UNSIGNED(feed, note->effect);
    SKIP_WHITESPACE(feed);
    if(*feed!='\n')
    {
        return NULL;
    }
    ctx->last_effect=note->effect;
    return ++feed;
}

static char* read_attribute(char* feed, struct sheet* sheet)
{
    unsigned i=0;
    SKIP_WHITESPACE(feed);
    if(strncmp(feed, "INSTRUMENT", 10)==0)
    {
        feed+=10;
        ENSURE_SKIP_WHITESPACE(feed);
        for(i=0;i<sizeof(instrument_names)/sizeof(const char**);i++)
        {
            if(strncmp(feed, instrument_names[i], strlen(instrument_names[i]))==0)
            {
                feed+=strlen(instrument_names[i]);
                /*Rivin pitää loppua tähän, muutoin virhe*/
                SKIP_WHITESPACE(feed);
                if(*feed!='\n')
                    return NULL;
                feed++;
                sheet->note_info.ins=(enum instrument)i;
                break;
            }
        }
    }
    else if(strncmp(feed, "UNIT", 4)==0)
    {
        feed+=4;
        ENSURE_SKIP_WHITESPACE(feed);
        
        READ_UNSIGNED_FRACTION(
            feed,
            sheet->note_info.unit_length_numer,
            sheet->note_info.unit_length_denom
        );
        SKIP_WHITESPACE(feed);
        if(*feed=='\n')
        {
            feed++;
        }
        else
        {
            return NULL;
        }
    }
    else
    {
        return NULL;
    }
    return feed;
}
static signed find_lowest_semitone(
    const struct proto_note* notes,
    unsigned notes_sz
)
{
    unsigned non_silent_notes=0;
    signed lowest=0;
    unsigned i=0;
    for(i=0;i<notes_sz;i++)
    {
        if(notes[i].silent==0&&(non_silent_notes==0||lowest>notes[i].semitone))
        {
            non_silent_notes++;
            lowest=notes[i].semitone;
        }
    }
    return lowest;
}
static signed find_first_unused_semitone(
    const struct proto_note* notes,
    unsigned notes_sz
)
{/*Kovin on naiivi algoritmi, mutta tuskin tässä latailussa kiire on. Nuotitkaan
   tuskin mielettömän korkealle menevät.*/
    unsigned i=0;
    unsigned res=0;
    for(i=0;i<notes_sz;i++)
    {
        if(res==notes[i].semitone)
        {
            res++;
            i=0;
            continue;
        }
    }
    return res;
}
unsigned sheet_read(
    struct sheet* sheet,
    const char* sheet_src,
    unsigned* error_line
){
    char *feed=(char*)malloc(strlen(sheet_src)+2);/*Tämä on muokattavaa*/
    char *feed_begin=feed;
    char *tmp=feed;/*Käytetään virheiden tunnistukseen*/
    struct proto_note *notes=NULL;
    unsigned notes_sz=0, notes_alloc=0;
    struct proto_note note;
    struct read_note_context ctx={0};
    unsigned line=0;
    unsigned i=0;
    signed lowest=0;
    
    /*Lisätään newline loppuun, niin funktioiden ei tarvitse tarkistaa
      nollaa*/
    sprintf(feed, "%s\n", sheet_src);
    /*Poistetaan koko lähteestä kommentit saman tien, että niitä ei tarvitse
      ajatella.*/
    remove_comments(feed);
    while(feed!=NULL&&*feed!=0)
    {
        line++;
        /*Skipataan sekä whitespace että newlinet*/
        SKIP_WHITESPACE(feed);
        if(*feed=='\n')
        {
            feed++;
            continue;
        }
        
        if((tmp=read_attribute(feed, sheet))!=NULL)
        {
            feed=tmp;
            continue;
        }
        else if((tmp=read_note(feed, &ctx, &note))!=NULL)
        {
            feed=tmp;
            if(notes_sz>=notes_alloc)
            {
                notes_alloc+=BUCKET_SIZE;
                notes=(struct proto_note*)realloc(notes, sizeof(struct proto_note)*notes_alloc);
            }
            notes[notes_sz]=note;
            notes_sz++;
            continue;
        }
        else
        {
            goto read_fail;
        }
    }
    /*Koko tiedosto on nyt luettu, ei saa epäonnistua enää!*/
    sheet->note_info.base_freq=0;
    sheet->notes_sz=notes_sz;
    sheet->notes=NULL;
    if(notes_sz==0)/*Ei nuotteja?*/
    {
        goto read_success;/*Technically correct, best kind of correct*/
    }
    sheet->notes=(struct note*)malloc(sizeof(struct note)*notes_sz);
    /*Etsitään matalin nuotti, jotta saadaan base_freq*/
    lowest=find_lowest_semitone(notes, notes_sz);
    sheet->note_info.base_freq=440*pow(2,lowest/12.);
    /*Siirretään joka nuotti siihen kohtaan, missä se olisi valmiissa muodossa*/
    for(i=0;i<notes_sz;i++)
    {
        notes[i].semitone-=lowest;
    }
    /*Sitten etsitään ensimmäinen käyttämätön nuotti, jota voidaan siis käyttää
      hiljaisuutena.*/
    sheet->note_info.silent_semitone=find_first_unused_semitone(notes, notes_sz);
    /*Kopioidaan nuotit loppusijoituspaikkaansa*/
    for(i=0;i<notes_sz;i++)
    {
        if(notes[i].silent==1)
        {
            sheet->notes[i].semitone=sheet->note_info.silent_semitone;
        }
        else
        {
            sheet->notes[i].semitone=notes[i].semitone;
        }
        /*Kyllä, pituus on aina automaattisesti absoluuttisessa muodossa.
          create_piece saa itse muuntaa nämä eksponentiaalisiksi, jos voi.*/
        sheet->notes[i].length=notes[i].length-1;
        sheet->notes[i].effect=notes[i].effect;
    }
read_success:
    if(notes!=NULL)
    {
        free(notes);
    }
    free(feed_begin);
    return 0;
read_fail:
    sheet->notes=NULL;/*Estetään free_sheet:iä kaatamasta ohjelmaa*/
    sheet->notes_sz=0;
    if(error_line!=NULL)
    {
        *error_line=line;
    }
    if(notes!=NULL)
    {
        free(notes);
    }
    free(feed_begin);
    return 1;
}

char *sheet_to_header(const struct sheet* sheet)
{/*TODO*/
    (void)sheet;
    return NULL;
}

unsigned sheet_convert_length_type(
    struct sheet* sheet,
    enum length_type src,
    enum length_type dst
)
{
    unsigned i=0;
    if(sheet_can_convert_length_type(sheet, src, dst)!=0)
        return 1;
    if(src==dst)
        return 0;
    if(src==LENGTH_EXP&&dst==LENGTH_ABS)
    {
        for(i=0;i<sheet->notes_sz;i++)
        {
            sheet->notes[i].length=(1<<sheet->notes[i].length)-1;
        }
        return 0;
    }
    else if(src==LENGTH_ABS&&dst==LENGTH_EXP)
    {
        for(i=0;i<sheet->notes_sz;i++)
        {
            sheet->notes[i].length=__builtin_ctz(sheet->notes[i].length+1);
        }
        return 0;
    }
    return 0;
}
/*Palauttaa ei-nollan, jos pituustyyppiä ei voi muuntaa.*/
unsigned sheet_can_convert_length_type(
    const struct sheet* sheet,
    enum length_type src,
    enum length_type dst
)
{
    if(src==dst||dst==LENGTH_ABS)
        return 0;
    if(src==LENGTH_ABS&&dst==LENGTH_EXP)
    {
        /*Katsotaan kaikki nuotit läpi.*/
        unsigned i=0;
        for(i=0;i<sheet->notes_sz;i++)
        {
            unsigned l=sheet->notes[i].length+1;
            if(l&(l-1))/*Testataan onko kahden potenssi*/
            {
                return 1;/*Ei ollut*/
            }
        }
        return 0;
    }
    return 1;
}

enum validate_error sheet_validate(
    const struct sheet* sheet,
    const struct note_format* format,
    const struct note_bits nb,
    unsigned* error_line
)
{
    unsigned i=0;
    enum validate_error err=VALIDATE_NO_ERROR;
    for(i=0;i<sheet->notes_sz;++i)
    {
        if((err=note_validate(sheet->notes[i], format, nb))!=VALIDATE_NO_ERROR)
        {
            *error_line=i;
            return err;
        }
    }
    return err;
}

float sheet_get_frequency(const struct sheet *sheet, unsigned index)
{
    unsigned s=sheet->notes[index%sheet->notes_sz].semitone;
    if(s==sheet->note_info.silent_semitone)
    {
        return 1;
    }
    else
    {
        return sheet->note_info.base_freq*pow(
            2.,
            sheet->notes[index%sheet->notes_sz].semitone/12.
        );
    }
}
const unsigned* sheet_get_semitone(
    const struct sheet* sheet,
    unsigned index
)
{
    const unsigned* s=&sheet->notes[index%sheet->notes_sz].semitone;
    if(*s==sheet->note_info.silent_semitone)
    {
        return NULL;
    }
    else
    {
        return s;
    }
}
/*Palauttaa nuotin 'index' pituuden sampleina.*/
unsigned sheet_get_length(
    const struct sheet* sheet,
    const struct note_format* fmt,
    unsigned samplerate,
    unsigned index
)
{
    if(sheet->notes_sz==0)
        return 0;
    return sheet->note_info.unit_length_numer*samplerate*note_get_length(
        sheet->notes[index%sheet->notes_sz],
        fmt
    )/sheet->note_info.unit_length_denom;
}
/*Palauttaa nuotin 'index' efektin. Älä yritä free:ata palautettua efektiä*/
union effect* sheet_get_effect(
    const struct sheet* sheet,
    const struct note_format* fmt,
    unsigned index
)
{
    return note_get_effect(sheet->notes[index%sheet->notes_sz], fmt);
}
void free_sheet(struct sheet *sheet)
{
    if(sheet->notes!=NULL)
    {
        free(sheet->notes);
        sheet->notes=NULL;
    }
}
