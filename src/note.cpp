/*
    Copyright 2015 Julius Ikkala

    This file is part of ByteTracker.

    ByteTracker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ByteTracker is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ByteTracker.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "note.h"
#include "effect.h"
#include <stdlib.h>

enum validate_error note_validate(
    struct note n,
    const struct note_format *fmt,
    const struct note_bits nb
)
{
    if(n.semitone>=(1<<nb.semitone_bits))
    {
        return VALIDATE_SEMITONE_LIMITS_EXCEEDED;
    }
    else if(n.effect>=(1<<nb.effect_bits))
    {
        return VALIDATE_EFFECT_LIMITS_EXCEEDED;
    }
    else if(n.length>=(1<<nb.length_bits))
    {
        return VALIDATE_LENGTH_LIMITS_EXCEEDED;
    }
    return VALIDATE_NO_ERROR;
}

union effect * note_get_effect(
    struct note n,
    const struct note_format *fmt
)
{
    unsigned i=0;
    /*Efekti menee yli rajojen, tai formaatti on rikki (varmaankin free:attu)*/
    if(fmt==NULL || fmt->effect_keys==NULL || fmt->effects==NULL)
        return NULL;
    
    for(i=0;i<fmt->effects_sz;++i)
    {
        if(n.effect==fmt->effect_keys[i])
        {
            return fmt->effects+i;
        }
    }
    return NULL;
}

unsigned note_get_length(
    struct note n,
    const struct note_format *fmt
)
{
    switch(fmt->len_type)
    {
    case LENGTH_ABS:
        return n.length+1;
    case LENGTH_EXP:
        return (1<<n.length);
    default:
        return 0;
    }
}

void note_format_set_effect(
    struct note_format *fmt,
    unsigned index,
    union effect eff
)
{
    unsigned i=0;
    /*Katsotaan, onko efektipaikalla jo joku toinen efekti*/
    for(i=0;i<fmt->effects_sz;++i)
    {
        if(index==fmt->effect_keys[i])
        {
            /*Näköjään on.*/
            free_effect(fmt->effects+i);
            fmt->effects[i]=eff;
            return;
        }
    }
    /*Ei ole, tehdään uusi efekti.*/
    fmt->effects_sz++;
    fmt->effects=(union effect *)realloc(
        fmt->effects,
        sizeof(union effect)*fmt->effects_sz
    );
    fmt->effect_keys=(unsigned *)realloc(
        fmt->effect_keys,
        sizeof(unsigned)*fmt->effects_sz
    );
    fmt->effects[fmt->effects_sz-1]=eff;
    fmt->effect_keys[fmt->effects_sz-1]=index;
}

void create_note_format(
    struct note_format *fmt,
    enum length_type len_type
)
{
    fmt->effects=NULL;
    fmt->effect_keys=NULL;
    fmt->effects_sz=0;
    fmt->len_type=len_type;
}

void free_note_format(struct note_format *fmt)
{
    if(fmt->effect_keys!=NULL)
    {
        free(fmt->effect_keys);
        fmt->effect_keys=NULL;
    }
    if(fmt->effects!=NULL)
    {
        unsigned i=0;
        for(i=0;i<fmt->effects_sz;++i)
        {
            free_effect(fmt->effects+i);
        }
        free(fmt->effects);
        fmt->effects=NULL;
    }
    fmt->effects_sz=0;
}
