/*
    Copyright 2015 Julius Ikkala

    This file is part of ByteTracker.

    ByteTracker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ByteTracker is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ByteTracker.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "instrument.h"
#include <math.h>
#include <stdlib.h>
#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif
#define min(a,b) ((a)<(b)?(a):(b))
#define max(a,b) ((a)>(b)?(a):(b))
#define clamp(a, mi, ma) max(min(a, ma), mi)

const char *const instrument_names[]={
    "SINE",
    "TRIANGLE",
    "SQUARE",
    "SAWTOOTH",
    "NOISE",
    "ORGAN",
    "DARKSYNTH"
};
static float saw(phase_type phase, float offset)
{
    double p=phase%1;
    return p<offset?1-2/offset*p:1-(2/(offset-1))*(p-1);
}

float instrument_sine(phase_type phase)
{
    return sin((phase%2)*2*M_PI);
}
float instrument_triangle(phase_type phase)
{
    return saw(phase, 0.5);
}
float instrument_square(phase_type phase)
{
    /*Ei, tämä ei ole bugi. Squaren amplitudi on tarkoituksella pienempi,
      koska sen voluumi kuulostaa suhteessa aina kovemmalta kuin sinin.*/
    return (double)((int)(phase)%2)*0.75-0.75/2;
}
float instrument_sawtooth(phase_type phase)
{
    return (double)(((phase*2)%2)-1)*0.5;
}
float instrument_noise(phase_type phase)
{
    return (rand()/(RAND_MAX/2)-1)*0.25;
}

float instrument_organ(phase_type phase)
{
    float f=0;
    unsigned i=0;
    float n=1;
    for(i=0;i<5;i++)
    {
        f+=instrument_sine(phase*n)/(n);
        n*=2;
    }
    return f;
}
float instrument_darksynth(phase_type phase)
{
    return instrument_sine(phase*0.992)*2+saw(phase, 0.15)+saw(phase*1.008, 0.15);
}
const instrument_fn instrument_fns[]={
    instrument_sine,
    instrument_triangle,
    instrument_square,
    instrument_sawtooth,
    instrument_noise,
    instrument_organ,
    instrument_darksynth
};

