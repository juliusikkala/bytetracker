/*
    Copyright 2015 Julius Ikkala

    This file is part of ByteTracker.

    ByteTracker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ByteTracker is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ByteTracker.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "gui.h"
#include <stdlib.h>
#include <limits.h>
#include <string.h>

struct gui_object
{
    enum gui_object_type type;

    struct gui_panel* owner;
    gui_event_callback fn;
    /*Käytetään oikean objektin tunnistukseen hiirieventeissä*/
    unsigned x, y, w, h;
    /*Käytetään objektin tunnistukseen callbackissa*/
    unsigned id;
};

struct gui_panel
{
    struct gui_object super;
    struct gui_object **objects;
    size_t objects_sz;
    struct gui_object *active_object;
    struct gui_theme* theme;
};

struct gui_button
{
    struct gui_object super;
    SDL_Texture* img;
    unsigned img_w, img_h;/*Ettei tarvitse querytä joka kerralla kun piirtää*/
    int pressed;
};

struct gui_image
{
    struct gui_object super;
    SDL_Texture* img;
};

static void panel_handle_event(struct gui_panel* p, union gui_event e)
{
    /*Hoitele scroll täällä sitten joskus*/
}
static void button_handle_event(struct gui_button* b, union gui_event e)
{
    switch(e.type)
    {
    case GUI_MOUSE_PRESS:
        b->pressed=1;
        break;
    case GUI_MOUSE_RELEASE:
    case GUI_MOUSE_LEAVE:
        b->pressed=0;
        break;
    default:
        break;
    }
}

static void image_handle_event(struct gui_image* i, union gui_event e)
{
}

static void object_handle_event(
    struct gui_object* obj,
    union gui_event e,
    void* userdata
)
{
    switch(obj->type)
    {
    case GUI_PANEL:
        panel_handle_event((struct gui_panel*) obj, e);
        break;
    case GUI_BUTTON:
        button_handle_event((struct gui_button*) obj, e);
        break;
    case GUI_IMAGE:
        image_handle_event((struct gui_image*) obj, e);
        break;
    }
    if(obj->fn!=NULL)
    {
        obj->fn(userdata, obj->id, e);
    }
}

static unsigned id_is_free(struct gui_panel* p, unsigned id)
{
    /*Ensin tarkistetaan tämä taso*/
    unsigned i=0;
    
    if(id==0)/*Nolla on varattu*/
    {
        return 0;
    }
    
    for(i=0;i<p->objects_sz;++i)
    {
        if(id==p->objects[i]->id)
        {
            return 0;
        }
    }
    /*Päästiin tähän asti, joten tämä taso on vapaa.*/
    return p->super.owner==NULL?1:id_is_free(p->super.owner, id);
}

static unsigned find_unused_id(struct gui_panel* p)
{
    unsigned id=1;/*Nolla on varattu*/
    
    for(id=1;id_is_free(p, id)==0&&id!=UINT_MAX;id++);
    
    if(id==UINT_MAX)
    {
        return 0;
    }
    
    return id;
}

static struct gui_object* find_object(struct gui_panel* p, unsigned x, unsigned y)
{
    unsigned j=0;
    if(p->objects_sz!=0&&p->objects!=NULL)
    {
        for(j=p->objects_sz;j>0;--j)
        {
            unsigned i=j-1;
            if(p->objects[i]->x<x&&
               p->objects[i]->y<y&&
               p->objects[i]->x+p->objects[i]->w>x&&
               p->objects[i]->y+p->objects[i]->h>y)
            {
                if(p->objects[i]->type==GUI_PANEL)
                {
                    return find_object(
                        (struct gui_panel*)p->objects[i],
                        x-p->objects[i]->x,
                        y-p->objects[i]->y
                    );
                }
                return p->objects[i];
            }
        }
    }
    return (struct gui_object*)p;
}
static void select_active_object(
    struct gui_panel* p,
    unsigned x,
    unsigned y,
    void* userdata
){
    struct gui_object* o=find_object(p, x, y);
    union gui_event e;
    if(o!=p->active_object)
    {
        if(p->active_object!=NULL)
        {
            e.type=GUI_MOUSE_LEAVE;
            object_handle_event(p->active_object, e, userdata);
        }
        p->active_object=o;
        e.type=GUI_MOUSE_ENTER;
        object_handle_event(o, e, userdata);
    }
}
static struct gui_theme* find_theme(struct gui_panel* p)
{
    return (p->theme==NULL?
               (p->super.owner==NULL?
                   NULL
                   :
                   find_theme(p->super.owner)
               )
               :
               p->theme
            );
}
static void set_colour(gui_colour col, SDL_Renderer* ren)
{
    SDL_SetRenderDrawColor(
        ren,
        (col>>24)&0xFF,
        (col>>16)&0xFF,
        (col>>8)&0xFF,
        col&0xFF
    );
}

static void object_draw(struct gui_object* obj, unsigned x0, unsigned y0, SDL_Renderer* ren);

static void panel_draw(struct gui_panel* p, unsigned x0, unsigned y0, SDL_Renderer* ren)
{
    unsigned i=0;
    struct gui_theme* t=find_theme(p);
    SDL_Rect rect;
    rect.x=p->super.x+x0;
    rect.y=p->super.y+y0;
    rect.w=p->super.w;
    rect.h=p->super.h;
    
    if(t!=NULL)
    {
        set_colour(t->panel_colour, ren);
        SDL_RenderFillRect(ren, &rect);
    }
    
    for(i=0;i<p->objects_sz;++i)
    {
        object_draw(p->objects[i], rect.x, rect.y, ren);
    }
}

static void button_draw(struct gui_button* b, unsigned x0, unsigned y0, SDL_Renderer* ren)
{
    struct gui_theme* t=find_theme(b->super.owner);
    SDL_Rect src, dst;
    
    if(t!=NULL)
    {
        image btn=(b->pressed==1?t->button.down:t->button.up);
        src.w=t->button.corner_w;
        src.h=t->button.corner_h;
        dst.w=src.w;
        dst.h=src.h;
        /*Vasen ylänurkka*/
        src.x=0;
        src.y=0;
        dst.x=b->super.x+x0;
        dst.y=b->super.y+y0;
        SDL_RenderCopy(ren, btn, &src, &dst);
        
        /*Oikea ylänurkka*/
        src.x=t->button.proto_w-src.w;
        src.y=0;
        dst.x=b->super.x+b->super.w+x0-src.w;
        dst.y=b->super.y+y0;
        SDL_RenderCopy(ren, btn, &src, &dst);
        
        /*Vasen alanurkka*/
        src.x=0;
        src.y=t->button.proto_h-src.h;
        dst.x=b->super.x+x0;
        dst.y=b->super.y+b->super.h+y0-src.h;
        SDL_RenderCopy(ren, btn, &src, &dst);

        /*Oikea alanurkka*/
        src.x=t->button.proto_w-src.w;
        src.y=t->button.proto_h-src.h;
        dst.x=b->super.x+b->super.w+x0-src.w;
        dst.y=b->super.y+b->super.h+y0-src.h;
        SDL_RenderCopy(ren, btn, &src, &dst);
        
        /*Ylä-vaakareuna*/
        src.x=t->button.corner_w;
        src.y=0;
        src.w=t->button.proto_w-t->button.corner_w*2;
        src.h=t->button.corner_h;
        dst.x=b->super.x+x0+t->button.corner_w;
        dst.y=b->super.y+y0;
        dst.w=b->super.w-t->button.corner_w*2;
        dst.h=src.h;
        SDL_RenderCopy(ren, btn, &src, &dst);
        
        /*Ala-vaakareuna*/
        src.y=t->button.proto_h-t->button.corner_h;
        dst.y=b->super.y+b->super.h+y0-src.h;
        SDL_RenderCopy(ren, btn, &src, &dst);
        
        /*Vasen pystyreuna*/
        src.x=0;
        src.y=t->button.corner_h;
        src.w=t->button.corner_w;
        src.h=t->button.proto_h-t->button.corner_h*2;
        dst.x=b->super.x+x0;
        dst.y=b->super.y+y0+t->button.corner_h;
        dst.w=src.w;
        dst.h=b->super.h-t->button.corner_h*2;
        SDL_RenderCopy(ren, btn, &src, &dst);
        
        /*Oikea pystyreuna*/
        src.x=t->button.proto_w-t->button.corner_w;
        dst.x=b->super.x+b->super.w+x0-src.w;
        SDL_RenderCopy(ren, btn, &src, &dst);
        
        /*Keskiö*/
        src.x=t->button.corner_w;
        src.y=t->button.corner_h;
        src.w=t->button.proto_w-t->button.corner_w*2;
        src.h=t->button.proto_h-t->button.corner_h*2;
        dst.x=b->super.x+x0+t->button.corner_w;
        dst.y=b->super.y+y0+t->button.corner_h;
        dst.w=b->super.w-t->button.corner_w*2;
        dst.h=b->super.h-t->button.corner_h*2;
        SDL_RenderCopy(ren, btn, &src, &dst);
        
        /*Kuva*/
        if(b->img!=NULL)
        {
            dst.w=b->img_w;
            dst.h=b->img_h;
            dst.x=b->super.x+x0+b->super.w/2-dst.w/2;
            dst.y=b->super.y+y0+b->super.h/2-dst.h/2;
            SDL_RenderCopy(ren, b->img, NULL, &dst);
        }
    }
}

static void image_draw(struct gui_image* p, unsigned x0, unsigned y0, SDL_Renderer* ren)
{
    SDL_Rect dst;
    dst.x=p->super.x+x0;
    dst.y=p->super.y+y0;
    dst.w=p->super.w;
    dst.h=p->super.h;
    SDL_RenderCopy(ren, p->img, NULL, &dst);
}

static void object_draw(struct gui_object* obj, unsigned x0, unsigned y0, SDL_Renderer* ren)
{
    switch(obj->type)
    {
    case GUI_PANEL:
        panel_draw((struct gui_panel*) obj, x0, y0, ren);
        break;
    case GUI_BUTTON:
        button_draw((struct gui_button*) obj, x0, y0, ren);
        break;
    case GUI_IMAGE:
        image_draw((struct gui_image*) obj, x0, y0, ren);
        break;
    }
}

static void free_panel(struct gui_panel* p)
{
    gui_panel_clear(p);
}

static void free_button(struct gui_button* b)
{

}
/*Nimi on workaround, täällä on jo free_image (sdl.h)*/
static void free_image_object(struct gui_image* i)
{
    /*Ei tehtävää*/
    (void)i;
}

static void free_object(struct gui_object* obj)
{
    switch(obj->type)
    {
    case GUI_PANEL:
        free_panel((struct gui_panel*) obj);
        break;
    case GUI_BUTTON:
        free_button((struct gui_button*) obj);
        break;
    case GUI_IMAGE:
        free_image_object((struct gui_image*) obj);
        break;
    }
}

/*Luo 'standalone' paneelin, id 0*/
struct gui_panel* gui_create_panel(
    unsigned w,
    unsigned h,
    struct gui_theme* t
)
{
    struct gui_panel* p=(struct gui_panel*)malloc(sizeof(struct gui_panel));
    p->super.type=GUI_PANEL;
    p->super.owner=NULL;
    p->super.fn=NULL;
    p->super.x=p->super.y=0;
    p->super.w=w;
    p->super.h=h;
    p->super.id=0;
    p->objects=NULL;
    p->objects_sz=0;
    p->active_object=(struct gui_object*)p;
    p->theme=t;
    return p;
}

void gui_free_panel(struct gui_panel* p){gui_panel_clear(p);}

struct gui_object* gui_panel_get(struct gui_panel* p, unsigned id)
{
    if(p->objects!=NULL)
    {
        unsigned i=0;
        for(i=0;i<p->objects_sz;++i)
        {
            if(p->objects[i]->id==id)
            {
                return p->objects[i];
            }
        }
    }
    return NULL;
}

void gui_panel_remove(struct gui_panel* p, unsigned id)
{
    if(p->objects!=NULL)
    {
        unsigned i=0;
        for(i=0;i<p->objects_sz;++i)
        {
            if(p->objects[i]->id==id)
            {
                if(p->active_object==p->objects[i])
                {
                    p->active_object=NULL;
                }
                free_object(p->objects[i]);
                free(p->objects[i]);
                p->objects_sz--;
                memmove(p->objects+i, p->objects+i+1, p->objects_sz-i);
                /*Säästetään muistia ja tuhlataan aikaa :)*/
                p->objects=(struct gui_object**)realloc(
                    p->objects,
                    p->objects_sz*sizeof(struct gui_object*)
                );
                break;
            }
        }
    }
}

static void gui_panel_push(struct gui_panel* p, struct gui_object* obj)
{
    /*Poistetaan, jos on jokin objekti jo tällä indeksillä*/
    gui_panel_remove(p, obj->id);
    p->objects_sz++;
    p->objects=(struct gui_object**)realloc(p->objects, p->objects_sz*sizeof(struct gui_object*));
    p->objects[p->objects_sz-1]=obj;
}

void gui_panel_clear(struct gui_panel* p)
{
    if(p->objects!=NULL)
    {
        unsigned i=0;
        for(i=0;i<p->objects_sz;++i)
        {
            free_object(p->objects[i]);
        }
        free(p->objects);
        p->objects=NULL;
        p->active_object=NULL;
    }
}

/*Palauttaa id:n, tai nollan virheessä.*/
unsigned gui_push_button(
    struct gui_panel* p,
    unsigned x, unsigned y,
    unsigned w, unsigned h,/*Aseta nollaan, jos haluat automaattisen leveyden*/
    gui_event_callback cb,/*Aseta NULL:iin jos haluat ilman callbackia*/
    unsigned id,/*Aseta nollaan, jos haluat funktion keksivän id:n itse*/
    SDL_Texture* img/*Lainataan*/
)
{
    struct gui_button* b=NULL;
    b=(struct gui_button*)malloc(sizeof(struct gui_button));
    
    if(img!=NULL&&image_get_size(img, &(b->img_w), &(b->img_h))!=0)
    {
        free(b);
        return 0;
    }
    b->super.type=GUI_BUTTON;
    b->super.owner=p;
    b->super.fn=cb;
    b->super.x=x;
    b->super.y=y;
    b->super.w=(w==0)?b->img_w:w;
    b->super.h=(h==0)?b->img_h:h;
    b->super.id=(id==0)?find_unused_id(p):id;
    b->pressed=0;
    b->img=img;
    
    gui_panel_push(p, (struct gui_object*)b);
    return b->super.id;
}

unsigned gui_push_panel(
    struct gui_panel* p,
    unsigned x, unsigned y,
    unsigned w, unsigned h,
    gui_event_callback cb,
    unsigned id
)
{
    struct gui_panel* o=NULL;

    o=(struct gui_panel*)malloc(sizeof(struct gui_panel));
    
    o->super.type=GUI_PANEL;
    o->super.owner=p;
    o->super.fn=cb;
    o->super.x=x;
    o->super.y=y;
    o->super.w=w;
    o->super.h=h;
    o->super.id=(id==0)?find_unused_id(p):id;
    o->objects=NULL;
    o->objects_sz=0;
    o->theme=NULL;/*Käyttää automaattisesti ownerin teemaa*/
    
    gui_panel_push(p, (struct gui_object*)o);
    return o->super.id;
}

unsigned gui_push_image(
    struct gui_panel* p,
    unsigned x, unsigned y,
    unsigned w, unsigned h,
    gui_event_callback cb,
    unsigned id,
    image img/*Lainataan*/
)
{
    struct gui_image* i=NULL;
    i=(struct gui_image*)malloc(sizeof(struct gui_image));
    
    if((w==0||h==0)&&image_get_size(img, &(i->super.w), &(i->super.h))!=0)
    {
        free(i);
        return 0;
    }
    i->super.type=GUI_IMAGE;
    i->super.owner=p;
    i->super.fn=cb;
    i->super.x=x;
    i->super.y=y;
    i->super.w=(w==0)?i->super.w:w;
    i->super.h=(h==0)?i->super.h:h;
    i->super.id=(id==0)?find_unused_id(p):id;
    i->img=img;
    
    gui_panel_push(p, (struct gui_object*)i);
    return i->super.id;
}

void draw_gui(struct gui_panel* panel, SDL_Renderer* ren)
{
    panel_draw(panel, 0, 0, ren);
}
    
void gui_handle_event(struct gui_panel* p, SDL_Event event, void* userdata)
{
    union gui_event e;
    switch(event.type)
    {
    case SDL_MOUSEMOTION:
        select_active_object(p, event.motion.x, event.motion.y, userdata);
        break;
    case SDL_MOUSEBUTTONDOWN:
    case SDL_MOUSEBUTTONUP:
        e.type=(event.type==SDL_MOUSEBUTTONDOWN?
                    GUI_MOUSE_PRESS:
                    GUI_MOUSE_RELEASE);
        switch(event.button.button)
        {
        case SDL_BUTTON_LEFT:
            e.mouse_button.btn=GUI_BUTTON_LEFT;
            break;
        case SDL_BUTTON_RIGHT:
            e.mouse_button.btn=GUI_BUTTON_RIGHT;
            break;
        default:
        case SDL_BUTTON_MIDDLE:
            e.mouse_button.btn=GUI_BUTTON_MIDDLE;
            break;
        }
        /*Ainakaan Windowsilla (ja varmaan joillakin WM:illä Linuxilla)
          MOUSEMOTION ei tule aina ennen MOUSEBUTTON* -eventtejä. Pitää siis
          hakea aktiivinen objekti, jotta active_object ei olisi NULL.*/
        select_active_object(p, event.button.x, event.button.y, userdata);
        object_handle_event(p->active_object, e, userdata);
        break;
    case SDL_MOUSEWHEEL:
        e.type=GUI_MOUSE_SCROLL;
        e.mouse_scroll.x=event.wheel.x;
        e.mouse_scroll.y=event.wheel.y;
        
        object_handle_event(p->active_object, e, userdata);
        break;
    default:
        break;
    }
}
