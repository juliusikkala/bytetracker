/*
    Copyright 2015 Julius Ikkala

    This file is part of ByteTracker.

    ByteTracker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ByteTracker is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ByteTracker.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "audio.h"
#include "piece.h"
#include "effect.h"
#include "instrument.h"
#include "sheet.h"
#include <SDL2/SDL.h>
#include <math.h>
#define SAMPLERATE 44100

struct audio_object
{
    SDL_AudioDeviceID dev;
    SDL_AudioSpec spec;
    struct piece* p;
    struct sheet_audio_info* sheet_audio_infos;
    struct audio_info info;
    float volume;
};

void audio_callback(void* userdata, Uint8* stream, int bytes)
{
    struct audio_object* obj=(struct audio_object*)userdata;
    unsigned i=0, j=0;
    float *d=(float*)stream;
    for(i=0;i<bytes/sizeof(float);i++)
    {
        d[i]=0.0f;
        for(j=0;j<obj->p->sheets_sz;j++)
        {
            struct sheet_audio_info* s=obj->sheet_audio_infos+j;
            struct sheet* sh=obj->p->sheets+j;
            if(s->note_time>=s->note_length)
            {/*Nuotti vaihtui*/
                s->note_time=0;
                s->note_index++;
                s->note_index%=sh->notes_sz;
                s->note_length=sheet_get_length(
                    sh,
                    &(obj->p->format),
                    obj->info.samplerate,
                    s->note_index
                );
            }
            
            d[i]+=get_sample(
                sheet_get_effect(
                    sh,
                    &(obj->p->format),
                    s->note_index
                ),
                sh,
                s,
                &obj->info
            );
            s->note_time++;
        }
        d[i]*=obj->volume;
        obj->info.t++;
    }
}

struct audio_object* begin_audio(struct piece* p)
{
    struct audio_object* obj=(struct audio_object*)malloc(sizeof(struct audio_object));
    SDL_AudioSpec request;
    
    SDL_zero(request);
    request.freq=SAMPLERATE;
    request.format=AUDIO_F32SYS;
    request.channels=1;
    request.samples=4096;
    request.callback=audio_callback;
    request.userdata=obj;
    obj->dev=SDL_OpenAudioDevice(
        NULL,
        0,
        &request,
        &obj->spec,
        SDL_AUDIO_ALLOW_FREQUENCY_CHANGE
    );
    if(obj->dev==0)
    {
        free(obj);
        return NULL;
    }
    obj->volume=0.5;
    obj->info.samplerate=obj->spec.freq;
    obj->p=p;
    obj->sheet_audio_infos=(struct sheet_audio_info*)malloc(sizeof(struct sheet_audio_info)*p->sheets_sz);

    rewind_audio_object(obj);
    return obj;
}

void pause_audio(struct audio_object* obj, unsigned pause)
{
    if(obj==NULL)
    {
        return;
    }
    SDL_PauseAudioDevice(obj->dev, pause);
}

void rewind_audio_object(struct audio_object* obj)
{
    unsigned i=0;
    if(obj==NULL)
    {
        return;
    }
    obj->info.t=0;
    for(i=0;i<obj->p->sheets_sz;i++)
    {
        obj->sheet_audio_infos[i].note_index=0;
        obj->sheet_audio_infos[i].note_time=0;
        obj->sheet_audio_infos[i].note_length=sheet_get_length(
            obj->p->sheets+i,
            &(obj->p->format),
            obj->info.samplerate,
            0
        );
        obj->sheet_audio_infos[i].phase=0;
        obj->sheet_audio_infos[i].last_sample=0;
    }
}

void audio_set_volume(struct audio_object* obj, float volume)
{
    if(obj==NULL)
    {
        return;
    }
    obj->volume=volume;
}

void stop_audio(struct audio_object* obj)
{
    if(obj==NULL)
    {
        return;
    }
    SDL_PauseAudioDevice(obj->dev, 1);
    SDL_CloseAudioDevice(obj->dev);
    free(obj->sheet_audio_infos);
}
float calculate_frequency(float base_freq, float semitone)
{
    return base_freq*pow(2, semitone/12.);
}
