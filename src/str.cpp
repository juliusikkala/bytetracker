/*
    Copyright 2015 Julius Ikkala

    This file is part of ByteTracker.

    ByteTracker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ByteTracker is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ByteTracker.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "str.h"
#include <string.h>
#include <stdlib.h>

void remove_comments(char* feed)
{
    char *line_start=NULL;
    char *multiline_start=NULL;
    char *comment_end=NULL;
    char *line_end=NULL;
    
    do
    {
        line_start=strstr(feed, LINE_COMMENT);
        multiline_start=strstr(feed, BEGIN_COMMENT);
        if(line_start!=NULL&&(line_start<multiline_start||multiline_start==NULL))
        {/*Yksirivinen kommentti*/
            line_end=strchr(line_start, '\n');
            memmove(
                line_start, line_end,
                strlen(line_end)+1
            );
        }
        else if(multiline_start!=NULL)
        {/*Useampirivinen.*/
            comment_end=strstr(
                multiline_start+strlen(BEGIN_COMMENT),
                END_COMMENT
            );
            comment_end+=strlen(END_COMMENT);
            /*Poistetaan rivi kerrallaan, jotta voidaan säilyttää newlinet*/
            while((line_end=strchr(multiline_start, '\n'))<comment_end)
            {
                comment_end-=line_end-multiline_start;
                memmove(
                    multiline_start, line_end,
                    strlen(line_end)+1
                );
                multiline_start++;/*Hypätään newlinen yli*/
            }
            /*Poistetaan loput kommentista.*/
            memmove(
                multiline_start, comment_end,
                strlen(comment_end)+1
            );
        }
    }
    while(line_start!=NULL||multiline_start!=NULL);
}
