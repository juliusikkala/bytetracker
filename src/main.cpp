/*
    Copyright 2015 Julius Ikkala

    This file is part of ByteTracker.

    ByteTracker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ByteTracker is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ByteTracker.  If not, see <http://www.gnu.org/licenses/>.
*/
#define SDL_MAIN_HANDLED
#include "sdl.h"
#include "io.h"
#include "sheet.h"
#include "piece.h"
#include "audio.h"
#include "gui.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <locale.h>
#define DEFAULT_WIDTH 320
#define DEFAULT_HEIGHT 192
#define BACKGROUND_ID 1
#define PLAY_BUTTON_ID 2
#define PAUSE_BUTTON_ID 3
#define REWIND_BUTTON_ID 4

struct player
{
    struct piece* p;
    struct audio_object* ao;
};

void handle_drop(struct player* p, char* path)
{
    char* src=NULL;
    char* path_prefix=NULL;
    unsigned error_line=0;
    unsigned error_sheet_index=0;
    unsigned error_sheet_line=0;
    unsigned len1=0, len2=0;
    
    if(p->p==NULL)
    {
        p->p=(piece*)malloc(sizeof(struct piece));
    }
    else
    {
        if(p->ao!=NULL)
        {
            stop_audio(p->ao);
            p->ao=NULL;
        }
        free_piece(p->p);
    }
    
    path_prefix=(char*)malloc(strlen(path));
    len1=strrchr(path, '/')-path+1;
    len2=strrchr(path, '\\')-path+1;
    sprintf(path_prefix, "%.*s", len1<len2?len1:len2, path);
    src=read_text_file(path);
    if(piece_read(
        p->p,
        src,
        path_prefix,
        &error_line,
        &error_sheet_index,
        &error_sheet_line)!=0)
    {
        char *error_msg=(char*)malloc(30);
        snprintf(error_msg, 30, "Syntaksivirhe rivillä %d", (error_line==0?error_sheet_line:error_line));
        SDL_ShowSimpleMessageBox(
            SDL_MESSAGEBOX_ERROR,
            "Virhe nuoteissa",
            error_msg,
            NULL
        );
        free(path_prefix);
        free(src);
        free(error_msg);
        free(p->p);
        p->p=NULL;
        return;
    }
    free(path_prefix);
    free(src);
    p->ao=begin_audio(p->p);
    audio_set_volume(p->ao, 0.1);
}
void button_callback(void* userdata, unsigned id, union gui_event e)
{
    struct player* p=(struct player*)userdata;
    if(e.type==GUI_MOUSE_PRESS)
    {
        switch(id)
        {
        case PLAY_BUTTON_ID:
            pause_audio(p->ao, 0);
            break;
        case PAUSE_BUTTON_ID:
            pause_audio(p->ao, 1);
            break;
        case REWIND_BUTTON_ID:
            rewind_audio_object(p->ao);
            break;
        }
    }
}
int main(int argc, char* argv[])
{
    struct sdl_resources res;
    struct gui_panel* p=NULL;
    struct gui_theme theme={0x8080FFFF, {NULL, NULL, 0,0,0,0}};
    image background_img=NULL;
    image play_img=NULL, pause_img=NULL, rewind_img=NULL;
    struct player player={NULL, NULL};
    SDL_Event event;
    int loop=1;
    
    setlocale(LC_ALL, "");
    if(init_sdl(&res, DEFAULT_WIDTH, DEFAULT_HEIGHT)!=0)
    {
        return 1;
    }
    set_window_icon(&res, "data/icon.png");
    background_img=load_image("data/background.png", &res);
    play_img=load_image("data/play.png", &res);
    pause_img=load_image("data/pause.png", &res);
    rewind_img=load_image("data/rewind.png", &res);
    theme.button.down=load_image("data/button_down.png", &res);
    theme.button.up=load_image("data/button_up.png", &res);
    image_get_size(
        theme.button.down,
        &theme.button.proto_w,
        &theme.button.proto_h
    );
    theme.button.corner_w=(theme.button.proto_w-1)/2;
    theme.button.corner_h=(theme.button.proto_h-1)/2;
    p=gui_create_panel(DEFAULT_WIDTH, DEFAULT_HEIGHT, &theme);
    gui_push_image(p, 0,0,DEFAULT_WIDTH,DEFAULT_HEIGHT,NULL, BACKGROUND_ID, background_img);
    gui_push_button(p, 10,149,40,40,button_callback,PLAY_BUTTON_ID, play_img);
    gui_push_button(p, 60,149,40,40,button_callback,PAUSE_BUTTON_ID, pause_img);
    gui_push_button(p, 110,149,40,40,button_callback,REWIND_BUTTON_ID, rewind_img);
    
    while(loop)
    {
        while(SDL_PollEvent(&event))
        {
            switch(event.type)
            {
            case SDL_QUIT:
                loop=0;
                break;
            /*Koska DROPFILE:ä ei voida luotettavasti paikallistaa, otetaan se
              koko ikkunan alueelta.*/
            case SDL_DROPFILE:
                handle_drop(&player, event.drop.file);
                SDL_free(event.drop.file);
                break;
            default:
                break;
            }
            gui_handle_event(p, event, &player);
        }
        draw_gui(p, res.ren);
        SDL_RenderPresent(res.ren);
        /*Varmuudeksi, ettei laita konetta polvilleen, vaikka vsync olisi
          pakotettu pois. Tämä on työkalu, ei peli, siksi prossun huudattaminen
          ei ole mielekästä tai odotettavaa käytöstä.*/
        SDL_Delay(0);
    }
    
    gui_free_panel(p);
    free_image(background_img);
    free_image(play_img);
    free_image(pause_img);
    free_image(rewind_img);
    free_image(theme.button.down);
    free_image(theme.button.up);
    quit_sdl(&res);
    
    return 0;
}
