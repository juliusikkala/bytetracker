/*
    Copyright 2015 Julius Ikkala

    This file is part of ByteTracker.

    ByteTracker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ByteTracker is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ByteTracker.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef BYTETRACKER_SHEET_H_
#define BYTETRACKER_SHEET_H_
    #include "instrument.h"
    #include "note.h"
    #include <stddef.h>
    
    struct sheet_note_info
    {
        float base_freq;
        unsigned silent_semitone;
        /*Koska sampleratea ei tiedetä, unit_length tallennetaan murtolukuna.*/
        unsigned unit_length_numer;
        unsigned unit_length_denom;
        enum instrument ins;
    };
    struct sheet
    {
        struct sheet_note_info note_info;
        struct note* notes;
        size_t notes_sz;
    };

    /*Luo struct sheet:in tekstitiedoston sisällöstä. jos löytää virheen,
      palauttaa ei-nollan ja kirjoittaa virheen rivin error_line:en. Huomaa,
      ettö sheet olettaa pituusformaatin olevan LENGTH_ABS. Jos näin ei ole,
      voit muuntaa sen haluttuun muotoon funktiolla sheet_convert_length_type*/
    /*Sheetin syntaksi on seuraava:
      ...
      ATTRIBUUTTI ARVO
      //kommentti
      NUOTTI:PITUUS:EFEKTI
      ...
      ATTRIBUUTTI on
      -INSTRUMENT
        -Arvo on instrumentin nimi, esim. SINE
      -UNIT
        -Arvo on yhden pituusyksikön pituus sekunneissa, voi olla murtoluku, muttei
         desimaaliluku.
      
      NUOTTI noudattaa seuraavaa kaavaa:
        [AHCDEFG][#b ][Oktaavin numero]
        Esim. A4 on 440Hz, A#4 on 466 jne.
        Erityisarvona koko nuotin voi jättää tyhjäksi. Tällöin saa hiljaisuuden.
      PITUUS on on nuotin pituus pituusyksikköinä
      EFEKTI on käytettävän efektin indeksi. Jos efekti jätetään pois, ohjelma
        käyttää indeksiä 00 tai edellisen nuotin efektiä.
    */
    unsigned sheet_read(
        struct sheet* sheet,
        const char* sheet_src,
        unsigned* error_line
    );
    
    /*Allokoi sopivan kokoisen char *, muista vapauttaa*/
    char *sheet_to_header(const struct sheet* sheet);
    
    /*Palauttaa ei-nollan, jos tulee virhe.*/
    unsigned sheet_convert_length_type(
        struct sheet* sheet,
        enum length_type src,
        enum length_type dst
    );
    /*Palauttaa ei-nollan, jos pituustyyppiä ei voi muuntaa.*/
    unsigned sheet_can_convert_length_type(
        const struct sheet* sheet,
        enum length_type src,
        enum length_type dst
    );
    
    /*Palauttaa ei-nollan ja kirjoittaa virheellisen nuotin error_note:en
      jos tulee virhe.*/
    enum validate_error sheet_validate(
        const struct sheet* sheet,
        const struct note_format* format,
        const struct note_bits nb,
        unsigned* error_note
    );
    
    /*Älä yritä vapauttaa palautettua arvoa. Palauttaa NULL, jos nuotti on
      hiljainen.*/
    const unsigned* sheet_get_semitone(
        const struct sheet* sheet,
        unsigned index
    );
    /*Palauttaa nuotin 'index' pituuden sampleina.*/
    unsigned sheet_get_length(
        const struct sheet* sheet,
        const struct note_format* fmt,
        unsigned samplerate,
        unsigned index
    );
    /*Palauttaa nuotin 'index' efektin. Älä yritä free:ata palautettua efektiä*/
    union effect* sheet_get_effect(
        const struct sheet* sheet,
        const struct note_format* fmt,
        unsigned index
    );

    void free_sheet(struct sheet* sheet);
#endif
