/*
    Copyright 2015 Julius Ikkala

    This file is part of ByteTracker.

    ByteTracker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ByteTracker is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ByteTracker.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef BYTETRACKER_PIECE_H_
#define BYTETRACKER_PIECE_H_
    #include <stdint.h>
    #include "note.h"
    
    struct sheet;
    struct piece
    {
        struct note_format format;
        struct sheet* sheets;
        unsigned sheets_sz;
    };
    
    
    /*Palauttaa ei-nollan, jos tulee virhe. Kirjoittaa virheen piece_error:iin*/
    unsigned create_piece(
        struct piece* piece,
        const char* const* sheet_paths,
        /*Lähteet, voi olla NULL. Sitten tämä funktio yrittää ladata itse
          sheet_paths:sista.*/
        const char* const* sheet_srcs,
        unsigned sheet_amount,
        unsigned* error_sheet_index,
        unsigned* error_line
    );
    /*Lataa piece:n  tekstitiedostosta. Palauttaa ei-nollan, jos tulee virhe.
      Kirjoittaa virheen piece_error:iin*/
    /*Piecen syntaksi on seuraava:
      ...
      ATTRIBUUTTI ARVO1 ARVO2 ... ARVON
      //kommentti
      ...
      ATTRIBUUTTI on
      -EFFECT
        -Arvot ovat: NUMERO TYYPPI ARGUMENTTI1 ARGUMENTTI2 ...
        -NUMERO on efektin numero sheeteissä
        -TYYPPI on efektin tyyppi
        -ARGUMENTTIN on tyypin vaatima argumentti.
      -SHEET
        -Arvo on sheet-tiedoston nimi ilman päätettä.
    */
    unsigned piece_read(
        struct piece* piece,
        const char* piece_src,
        const char* path_prefix,/*Jos NULL, käyttää ohjelman suorituskansiota*/
        unsigned* error_line,
        unsigned* error_sheet_index,
        unsigned* error_sheet_line
    );
    
    /*Vapauttaa kaiken piece:n käyttämän muistin. Kutsu tämä, kun pieceä ei enää
      käytetä*/
    void free_piece(struct piece* piece);
    
    enum validate_error piece_validate(
        struct piece* piece,
        unsigned* error_sheet_index,
        const struct note_bits nb,
        unsigned* error_line
    );
#endif
