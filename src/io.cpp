/*
    Copyright 2015 Julius Ikkala

    This file is part of ByteTracker.

    ByteTracker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ByteTracker is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ByteTracker.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "io.h"
#include <stdio.h>
#include <stdlib.h>
/*Muista free:ata palautettu string!*/
char *read_text_file(const char *path)
{
    char *res=NULL;
    unsigned file_sz=0;
    FILE *f=fopen(path, "rb");
    if(f==NULL)
        return NULL;
    
    fseek(f, 0, SEEK_END);
    file_sz=ftell(f);
    rewind(f);
    
    res=(char*)malloc(file_sz+1);
    res[file_sz]=0;/*String päättyy nollaan*/
    
    fread(res, 1, file_sz, f);
    
    fclose(f);
    return res;
}
