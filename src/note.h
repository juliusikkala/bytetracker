/*
    Copyright 2015 Julius Ikkala

    This file is part of ByteTracker.

    ByteTracker is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ByteTracker is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ByteTracker.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef BYTETRACKER_NOTE_H_
#define BYTETRACKER_NOTE_H_
    #include <stdlib.h>
    union effect;
    
    enum length_type
    {
        LENGTH_ABS=0,
        LENGTH_EXP
    };

    enum validate_error
    {
        VALIDATE_NO_ERROR=0,
        VALIDATE_SEMITONE_LIMITS_EXCEEDED,
        VALIDATE_LENGTH_LIMITS_EXCEEDED,
        VALIDATE_EFFECT_LIMITS_EXCEEDED
    };

    /*Kertoo nuotin tyypin dekoodauksessa*/
    struct note_format
    {
        union effect *effects;
        unsigned *effect_keys;
        size_t effects_sz;
        enum length_type len_type;
    };
    
    /*Käytetään validaatiossa*/
    struct note_bits
    {
        unsigned semitone_bits;
        unsigned effect_bits;
        unsigned length_bits;
    };

    /*"Rajaton" nuottityyppi*/
    struct note
    {
        unsigned semitone;
        unsigned effect;
        unsigned length;
    };

    /*Palauttaa VALIDATE_NO_ERROR:in eli nollan vain jos nuotti oli virheetön.*/
    enum validate_error note_validate(
        struct note n,
        const struct note_format *fmt,
        const struct note_bits nb
    );
    
    /*Älä yritä free:ata union effect:in sisälmyksiä, vaikka siellä pointteri
      olisikin. Jos efektiä ei löydy, palauttaa NULL:in*/
    union effect * note_get_effect(
        struct note n,
        const struct note_format *fmt
    );
    
    /*Pituus yksiköissä*/
    unsigned note_get_length(
        struct note n,
        const struct note_format *fmt
    );
    
    void note_format_set_effect(
        struct note_format *fmt,
        unsigned index,
        union effect eff/*Ottaa omistajuuden, älä free:aa jäseniä.*/
    );
    
    /*Efektit initialisoidaan erikseen note_format_set_effect:illä.*/
    void create_note_format(
        struct note_format *fmt,
        enum length_type len_type
    );

    void free_note_format(struct note_format *fmt);
#endif
